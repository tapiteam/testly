# Testly #

Testly is a framework for testing external Meteor.js instance.

### Methods ###

* Spawn N sessions on external Meteor.js instance
* Fire a method on external Meteor.js instance by N users in asynchronous time
* Subscribe a publication on external Meteor.js instance by N users in asynchronous time
* Check if collections have correctly allow/deny values. User can choose the name of the collection, number of tests and set if ```insert && update && remove``` should be ```allow``` or ```deny```. After that, framework tries to ```insert || update || remove``` and shows the results.
* Fire ```insert || remove || update``` on collection.

### Tests ###

All tests were made on another Meteor.js instance using the node-ddp npm module.

### Author ###

Dariusz Zabrzeński