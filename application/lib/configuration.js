App = {}
Users = []
Publications = []
Collections = []
Methods = []

App.config = {
    host : "application",
    port : 3000,
    ssl  : false,
    autoReconnect : true,
    autoReconnectTimer : 500,
    maintainCollections : true,
    ddpVersion : '1',
    useSockJs: true
}
