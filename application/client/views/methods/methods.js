/*
╔═════════════════════════════════════╗
      .♥. Methods template .♥.
╚═════════════════════════════════════╝
*/

Template.methods.helpers({
    methods() {
        var methods = Methods.find({}, {limit: 20}).fetch()

        _.each(methods, (methods) => {
            methods.miliseconds = methods.miliseconds/1000
        })

        return methods
    },
    count() {
        return Methods.find().count()
    },
    average() {
        var rows = Methods.find().fetch(),
            time = 0

        _.each(rows, (row) => {
            time += row.miliseconds
        })

        return time == 0 ? 0 : ((time/rows.length)/1000).toFixed(3)
    },
    listOfMethods() {
        return Config.findOne({ type: 'external' }).methods
    }
})

Template.methods.events({
    'click .new' (event) {
        event.preventDefault()

        $('.options').hide()
        $('.add-more').show()
    },
    'click .close' (event) {
        event.preventDefault()

        $('.add-more').hide()
        $('.options').show()
    },
    'click .fire' (event) {
        event.preventDefault()

        var method = $('select[name=methods]').val()
        var sessions = $('select[name=sessions-count]').val()

        $('.loader').show()

        Meteor.call('fireMethod', { method: method, sessions: sessions }, (err, data) => {
            if(err) {
                alert(err.reason)
            }

            Meteor.call('addMethodsHistory', data)
            $('.loader').hide()
        })
    },
    'click .remove-all' (event) {
        Meteor.call('removeMethods')
    },
    'click .remove-this' (event) {
        Meteor.call('removeMethod', this._id)
    }
})
