/*
╔═════════════════════════════════════╗
      .♥. Sessions template .♥.
╚═════════════════════════════════════╝
*/

Template.sessions.helpers({
    sessions() {
        var sessions = Sessions.find({}, {limit: 20}).fetch()

        _.each(sessions, (session) => {
            session.time = moment(session.time).calendar()
        })

        return sessions
    },
    count() {
        return Sessions.find().count()
    }
})

Template.sessions.events({
    'click .new' (event) {
        event.preventDefault()

        $('.options').hide()
        $('.add-more').show()
    },
    'click .close' (event) {
        event.preventDefault()

        $('.add-more').hide()
        $('.options').show()
    },
    'click .fire' (event) {
        event.preventDefault()

        var sessions = $('select[name=sessions-count]').val()

        $('.loader').show()

        Meteor.call('newSession', sessions, (err, data) => {
            if(err) {
                // doing something
            }

            $('.loader').hide()
        })
    },
    'click .remove-all' (event) {
        Meteor.call('removeSessions')
    },
    'click .remove-this' (event) {
        Meteor.call('removeSession', this.name)
    }
})
