/**
  * ♥ window.alert ♥
  * Change the default alert action with css/jquery alert
  *
  * @param {String} msg
  * @param {Bool}	result
*/

window.alert = function(msg, result)
{
    /** @type {String} */
    var class_name 	= result ? "alert_true" : "alert"

    /** @type {Object} */
    var alert = $("body").find("."+class_name)

    // Remove existing div if it's exists
    if(alert.length)
    {
        alert.remove()
    }

    // Append alert to application
    $("body").append('<div class="'+class_name+'">'+msg+'</div>')

    $("."+class_name).show().animate({
        bottom: "+=30",
        opacity: "1"
    },300,function() {
        $(this).animate({
            opacity: "0"
        },5000,function() {
            $(this).hide().html("").css("top","0px")
        })
    })
}
