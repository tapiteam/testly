/*
╔═════════════════════════════════════╗
      .♥. Collections template .♥.
╚═════════════════════════════════════╝
*/

Template.collections.helpers({
    collections() {
        var rows = Collections.find({}, {limit: 20}).fetch()
        return rows
    },
    count() {
        return Collections.find().count()
    },
    average() {
        var rows = Collections.find().fetch(),
            time = 0

        _.each(rows, (row) => {
            time += row.miliseconds
        })

        return time == 0 ? 0 : ((time/rows.length)/1000).toFixed(3)
    },
    listOfCollections() {
        return Config.findOne({ type: 'external' }).collections
    }
})

Template.collections.events({
    'click .new' (event) {
        event.preventDefault()

        $('.options').hide()
        $('.add-more').show()
    },
    'click .close' (event) {
        event.preventDefault()

        $('.add-more').hide()
        $('.options').show()
    },
    'click .fire' (event) {
        event.preventDefault()

        let options  = {}

        options.collection = $('select[name=collections]').val()
        options.sessions = $('select[name=sessions-count]').val()
        options.insert = $('select[name=insert]').val()
        options.update = $('select[name=update]').val()
        options.remove = $('select[name=remove]').val()

        $('.loader').show()

        Meteor.call('collectionTest', options, (err, data) => {
            if(err) {
                alert(err.reason)
            }

            Meteor.call('addCollectionsHistory', data)
            $('.loader').hide()
        })
    },
    'click .remove-all' (event) {
        Meteor.call('removeCollections')
    },
    'click .remove-this' (event) {
        Meteor.call('removeCollection', this._id)
    }
})
