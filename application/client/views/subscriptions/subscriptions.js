/*
╔═════════════════════════════════════╗
     .♥. Subscriptions template .♥.
╚═════════════════════════════════════╝
*/

Template.subscriptions.helpers({
    subscriptions() {
        var subs = Subscriptions.find({}, {limit: 20}).fetch()

        _.each(subs, (sub) => {
            sub.miliseconds = sub.miliseconds/1000
        })

        return subs
    },
    count() {
        return Subscriptions.find().count()
    },
    average() {
        var rows = Subscriptions.find().fetch(),
            time = 0

        _.each(rows, (row) => {
            time += row.miliseconds
        })

        return time == 0 ? 0 : ((time/rows.length)/1000).toFixed(3)
    },
    listOfPublications() {
        return Config.findOne({ type: 'external' }).publications
    }
})

Template.subscriptions.events({
    'click .new' (event) {
        event.preventDefault()

        $('.options').hide()
        $('.add-more').show()
    },
    'click .close' (event) {
        event.preventDefault()

        $('.add-more').hide()
        $('.options').show()
    },
    'click .fire' (event) {
        event.preventDefault()

        var publication = $('select[name=publications]').val()
        var sessions = $('select[name=sessions-count]').val()

        $('.loader').show()

        Meteor.call('addSubscribe', { publication: publication, sessions: sessions }, (err, data) => {
            if(err) {
                alert(err.reason)
            }

            Meteor.call('addSubscribtionsHistory', data)

            $('.loader').hide()
        })
    },
    'click .remove-all' (event) {
        Meteor.call('removeSubscribes')
    },
    'click .remove-this' (event) {
        Meteor.call('removeSubscribe', this._id)
    }
})
