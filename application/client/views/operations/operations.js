/*
╔═════════════════════════════════════╗
      .♥. Operations template .♥.
╚═════════════════════════════════════╝
*/

Template.operations.helpers({
    operations() {
        var rows = Operations.find({}, {limit: 20}).fetch()
        return rows
    },
    count() {
        return Operations.find().count()
    },
    average() {
        var rows = Operations.find().fetch(),
            time = 0

        _.each(rows, (row) => {
            time += row.miliseconds
        })

        return time == 0 ? 0 : ((time/rows.length)/1000).toFixed(3)
    },
    listOfCollections() {
        return Config.findOne({ type: 'external' }).collections
    }
})

Template.operations.events({
    'click .new' (event) {
        event.preventDefault()

        $('.options').hide()
        $('.add-more').show()
    },
    'click .close' (event) {
        event.preventDefault()

        $('.add-more').hide()
        $('.options').show()
    },
    'click .fire' (event) {
        event.preventDefault()

        let options  = {}

        options.collection = $('select[name=collections]').val()
        options.sessions = $('select[name=sessions-count]').val()
        options.method = $('select[name=method]').val()

        $('.loader').show()

        Meteor.call('operationsTest', options, (err, data) => {
            if(err) {
                alert(err.reason)
            }

            Meteor.call('addOperationsHistory', data)
            $('.loader').hide()
        })
    },
    'click .remove-all' (event) {
        Meteor.call('removeOperations')
    },
    'click .remove-this' (event) {
        Meteor.call('removeOperation', this._id)
    }
})
