import DDP from 'ddp'
import Future from 'fibers/future'
import async from 'async'

Meteor.methods({
    fireMethod: (data) => {
        let future = new Future(),
            count = 0,
            results = []

        if(Users.length === 0 || Users.length < data.sessions) {
            throw new Meteor.Error(422, 'Nie posiadasz wystarczającej liczby sesji')
        }

        var methodCall = function(id, callback) {
            let user = Users[id]
            user.startDate = new Date().getTime()

            user.call(
                data.method,
                [user.name],
                function (err, result) {
                    var thisUser =  _.find(Users, (user) => { return user.name = result })

                    results.push({
                        name: thisUser.name,
                        method: data.method,
                        miliseconds: moment(new Date().getTime()).diff(moment(thisUser.startDate), 'ms'),
                        result: result
                    })

                    count++

                    if(count == data.sessions) {
                        future.return(results)
                    }
                }
            )

            // callback at the same time
            callback(null, {})
        }

        async.times(data.sessions, function(n, next){
            methodCall(n, function(err, user) {
                next(err, user)
            })
        })

        return future.wait()
    },
    addMethodsHistory: (results) => {
        _.each(results, (result) => {
            Methods.insert(result)
        })

        return true
    },
    removeMethod: (_id) => {
        return Methods.remove(_id)
    },
    removeMethods: () => {
        return Methods.remove({})
    }
})
