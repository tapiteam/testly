import DDP from 'ddp'
import Future from 'fibers/future'
import async from 'async'

Meteor.methods({
    addSubscribe: (data) => {
        let future = new Future(),
            count = 0,
            results = []

        if(Users.length === 0 || Users.length < data.sessions) {
            throw new Meteor.Error(422, 'Nie posiadasz wystarczającej liczby sesji')
        }

        var fireSubscribe = function(id, callback) {
            let user = Users[id]
            user.startDate = new Date().getTime()

            user.subscribe(
                data.publication,
                [],
                function () {
                    results.push({
                        name: user.name,
                        publication: data.publication,
                        miliseconds: moment(new Date().getTime()).diff(moment(user.startDate), 'ms'),
                        result: user.collections[data.publication]
                    })

                    count++

                    if(count == data.sessions) {
                        future.return(results)
                    }
                }
            )

            // callback at the same time
            callback(null, {})
        }

        async.times(data.sessions, function(n, next){
            fireSubscribe(n, function(err, user) {
                next(err, user)
            })
        })

        return future.wait()
    },
    addSubscribtionsHistory: (results) => {
        _.each(results, (result) => {
            Subscriptions.insert(result)
        })

        return true
    },
    removeSubscribe: (_id) => {
        return Subscriptions.remove(_id)
    },
    removeSubscribes: () => {
        return Subscriptions.remove({})
    }
})
