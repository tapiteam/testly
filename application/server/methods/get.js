import DDP from 'ddp'
import Future from 'fibers/future'
import async from 'async'

Meteor.methods({
    getDataFromExternalApp: (data) => {
        let future = new Future()

        var p1 = new Promise(function(resolve, reject) {
            if(Users.length === 0) {
                Meteor.call('newSession', 1, function(err, data) {
                    resolve(true)
                })
            } else { resolve(true) }
        })

        p1.then(function() {
            let user = Users[0]

            user.call(
                'testlyGetData',
                [],
                function (err, result) {
                    future.return(result)
                }
            )
        })

        return future.wait()
    },
    saveDataFromExternalApp: (data) => {
        Config.upsert({ type: 'external' },
        {
            type: 'external',
            publications: data.publications,
            methods: data.methods,
            collections: data.collections
        })

        return true;
    }
})
