import DDP from 'ddp'
import Future from 'fibers/future'
import async from 'async'

Meteor.methods({
    collectionTest: (data) => {
        let future = new Future(),
            count = 0,
            results = []

        if(Users.length === 0 || Users.length < data.sessions) {
            throw new Meteor.Error(422, 'Nie posiadasz wystarczającej liczby sesji')
        }

        var call = function(id, callback) {
            let _id,
                user = Users[id],
                document = {}

            var p1 = new Promise(function(resolve, reject) {
                user.call(
                    'testlyInsert',
                    [{collection: data.collection}],
                    function(err, result) {
                        _id = result
                        resolve(true)
                    }
                )
            })

            function call(user, method, object, type, resolve) {
                user.call(method, object, function(err, result) {
                    if(typeof err !== 'undefined') {
                        document[type] = false
                    } else {
                        document[type] = true
                    }

                    resolve(true)
                })
            }

            p1.then(function() {
                var p2 = new Promise(function(resolve, reject) {
                    call(user,
                         '/'+data.collection.toLowerCase()+'/update',
                         [{ _id: _id}, {$set: {user: user.name}}],
                         'update',
                         resolve)
                })

                var p3 = new Promise(function(resolve, reject) {
                    call(user,
                         '/'+data.collection.toLowerCase()+'/remove',
                         [{_id: _id}],
                         'remove',
                         resolve)
                })

                var p4 = new Promise(function(resolve, reject) {
                    call(user,
                         '/'+data.collection.toLowerCase()+'/insert',
                         [{user: user.name}],
                         'insert',
                         resolve)
                })

                Promise.all([p2, p3, p4]).then(function(values) {
                    count++

                    // set new result
                    document.collection = data.collection
                    document.name = user.name
                    document.expectedInsert = data.insert == "true" ? true : false
                    document.expectedUpdate = data.update == "true" ? true : false
                    document.expectedRemove = data.remove == "true" ? true : false

                    results.push(document)

                    if(count == data.sessions) {
                        future.return(results)
                    }

                    callback(null, {})
                })
            })
        }

        async.times(data.sessions, function(n, next){
            call(n, function(err, user) {
                next(err, user)
            })
        })

        return future.wait()
    },
    addCollectionsHistory: (results) => {
        _.each(results, (result) => {
            Collections.insert(result)
        })

        return true
    },
    removeCollection: (_id) => {
        return Collections.remove(_id)
    },
    removeCollections: () => {
        return Collections.remove({})
    }
})
