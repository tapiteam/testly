import DDP from 'ddp'
import Future from 'fibers/future'
import async from 'async'

Meteor.methods({
    operationsTest: (data) => {
        let future = new Future(),
            count = 0,
            results = []

        if(Users.length === 0 || Users.length < data.sessions) {
            throw new Meteor.Error(422, 'Nie posiadasz wystarczającej liczby sesji')
        }

        var call = function(id, callback) {
            let _id,
                user = Users[id],
                document = {}

            function call(user, method, object, resolve) {
                user.call(method, object, function(err, result) {
                    if(typeof err !== 'undefined') {
                        document.result = false
                    } else {
                        document.result = true
                    }
                    
                    resolve(true)
                })
            }

            var p1 = new Promise(function(resolve, reject) {
                if(data.method == 'insert')
                {
                    call(user,
                         '/'+data.collection.toLowerCase()+'/insert',
                         [{user: user.name}],
                         resolve)
                } else {
                    user.call(
                        'testlyInsert',
                        [{collection: data.collection}],
                        function(err, result) {
                            _id = result
                            resolve(true)
                        }
                    )
                }
            })

            p1.then(function() {
                var p2

                if(data.method == 'insert')
                {
                    p2 = new Promise(function(resolve, reject) { resolve(true) })
                }
                else if(data.method == 'update')
                {
                    p2 = new Promise(function(resolve, reject) {
                        call(user,
                             '/'+data.collection.toLowerCase()+'/update',
                             [{ _id: _id}, {$set: {user: user.name}}],
                             resolve)
                    })
                } else if(data.method == 'remove')
                {
                    p2 = new Promise(function(resolve, reject) {
                        call(user,
                             '/'+data.collection.toLowerCase()+'/remove',
                             [{_id: _id}],
                             resolve)
                    })
                }

                p2.then(function() {
                    count++

                    // set new result
                    document.collection = data.collection
                    document.name = user.name
                    results.push(document)

                    if(count == data.sessions) {
                        future.return(results)
                    }

                    callback(null, {})
                })
            })
        }

        async.times(data.sessions, function(n, next){
            call(n, function(err, user) {
                next(err, user)
            })
        })

        return future.wait()
    },
    addOperationsHistory: (results) => {
        _.each(results, (result) => {
            Operations.insert(result)
        })

        return true
    },
    removeOperation: (_id) => {
        return Operations.remove(_id)
    },
    removeOperations: () => {
        return Operations.remove({})
    }
})
