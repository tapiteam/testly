import DDP from 'ddp'
import Future from 'fibers/future'

function randomString() {
    return Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 10)
};

Meteor.methods({
    newSession: (number) => {
        let future = new Future(),
            count = 0

        for(var i=0; i<number; i++)
        {
            let user = new DDP(App.config),
                data = {
                    name : 'user-'+randomString(),
                    time: new Date(),
                    status: 'online',
                    data: 'test'
                }

            user.connect((error, wasReconnect) => {
                count++

                if(count == number) {
                    future.return(data)
                }
            })

            // add information to object
            user.name = data.name

            // add user to Array
            Users.push(user)

            // add User to collection
            Sessions.insert(user)
        }

        return future.wait()
    },
    removeSession: (name) => {
        // find user
        let user = _.find(Users, (user) => { return user.name = name })

        // close socket connection
        user.close()

        // remove user session from array
        Users = _.without(Users, _.findWhere(Users, {name: name}))

        // remove session from database
        return Sessions.remove({ name: name })
    },
    removeSessions: () => {
        _.each(Users, (user) => {
            user.close()
        })

        Users = []

        return Sessions.remove({})
    },
    removeSessionsAtStart: () => {
        if(Users.length === 0)
        {
            return Sessions.remove({})
        }

        return true
    }
})
