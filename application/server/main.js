import { Meteor } from 'meteor/meteor'

Meteor.startup(function () {
    // Remove sessions at start
    Meteor.call('removeSessionsAtStart')

    // Get data from external application
    Meteor.call('getDataFromExternalApp', function(err, data) {
        Meteor.call('saveDataFromExternalApp', data)
    })
})
