/*
╔═════════════════════════════════════╗
       .♥. Iron-Router config .♥.
╚═════════════════════════════════════╝
*/

Router.configure({
    layoutTemplate: 'layout',
    loadingTemplate: 'loading'
});
