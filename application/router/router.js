/*
╔═════════════════════════════════════╗
       .♥. Iron-Router routes .♥.
╚═════════════════════════════════════╝
*/


Router.map(function() {
    this.route('sessions', {
        path: '/'
    });

    this.route('methods', {
        path: '/methods'
    });

    this.route('subscriptions', {
        path: '/subscriptions'
    });

    this.route('collections', {
        path: '/collections'
    });

    this.route('operations', {
        path: '/operations'
    });
});
